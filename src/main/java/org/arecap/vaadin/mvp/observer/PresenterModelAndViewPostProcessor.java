/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp.observer;


import org.arecap.vaadin.mvp.PassiveView;
import org.arecap.vaadin.mvp.ViewPresenter;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public interface PresenterModelAndViewPostProcessor {

    default void prepareModelAndView(ViewPresenter presenter, String state) {
        PassiveView view = presenter.getView();
        if(BeforeBindingPassiveViewObserver.class.isAssignableFrom(view.getClass())) {
            ((BeforeBindingPassiveViewObserver)view).beforeBinding();
        }
        if(ViewPresenterPrepareModelObserver.class.isAssignableFrom(presenter.getClass())) {
            ((ViewPresenterPrepareModelObserver)presenter).prepareModel(state);
        }
        if(PassiveViewPrepareBindingObserver.class.isAssignableFrom(view.getClass())) {
            ((PassiveViewPrepareBindingObserver)view).prepareBinding();
        }
        if(ViewPresenterAfterPrepareModelObserver.class.isAssignableFrom(presenter.getClass())) {
            ((ViewPresenterAfterPrepareModelObserver)presenter).afterPrepareModel(state);
        }
        if(PassiveViewAfterBindingObserver.class.isAssignableFrom(view.getClass())) {
            ((PassiveViewAfterBindingObserver)view).afterBinding();
        }
    }


}
