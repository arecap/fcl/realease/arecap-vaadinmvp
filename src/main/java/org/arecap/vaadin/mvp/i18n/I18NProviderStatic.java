/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp.i18n;

import com.vaadin.flow.component.UI;
import org.arecap.cop.BeanUtil;
import org.arecap.vaadin.mvp.boot.I18NProvider;

import java.util.Locale;

/**
 *
 * @autor Octavian Stirbei
 *
 * @author George Boboc
 * @since 1.0.0
 *
 */
public class I18NProviderStatic {

	public static String getTranslation(String key, Object... params) {
		Locale locale = UI.getCurrent().getLocale();
		return getTranslation(key, locale, params);
	}
	
	public static String getTranslation(String key, Locale locale, Object... params) {
		return ((I18NProvider) BeanUtil.getBean(I18NProvider.class)).getTranslation(key, locale, params);
	}
}
