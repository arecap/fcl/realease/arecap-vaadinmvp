/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp.i18n;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
@Tag("table")
public class TableFormI18n extends HtmlContainer {


    private HtmlContainer tableBody = new HtmlContainer("tbody");

    private final Map<Component, String> formRowInputs;

    public TableFormI18n() {
        this(new LinkedHashMap<>());

    }

    public TableFormI18n(Map<Component, String> formRowInputs) {
        this.formRowInputs = formRowInputs;
        buildTableBody();
        add(tableBody);
    }


    public void addFormRow(Component component, String label) {
        formRowInputs.put(component, label);
        buildTableRow(component);
    }

    private void buildTableBody() {
        tableBody.removeAll();
        formRowInputs.keySet().forEach(this::buildTableRow);
    }

    private void buildTableRow(Component component) {
        HtmlContainer tableRowLabel = new HtmlContainer("th");
        tableRowLabel.setText(I18NProviderStatic.getTranslation(formRowInputs.get(component)));
        HtmlContainer tableRowDetail = new HtmlContainer("td");
        tableRowDetail.add(component);
        tableBody.add(new HtmlContainer("tr", tableRowLabel, tableRowDetail));
    }


}
