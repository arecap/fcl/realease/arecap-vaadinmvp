/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp;

import com.vaadin.flow.component.Component;

import java.lang.reflect.ParameterizedType;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public interface PassiveView<P extends ViewPresenter> {

    P getPresenter();

    default void init(String instanceId, P presenter) {
        presenter.setView(this);
        if(Component.class.isAssignableFrom(getClass())) {
            ((Component)this).setId(instanceId);
        }
        if(PassiveViewBuilder.class.isAssignableFrom(getClass())) {
            ((PassiveViewBuilder)this).buildView(presenter);
        }
    }

    default Class<P> getPresenterType() {
        return (Class<P>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

}
