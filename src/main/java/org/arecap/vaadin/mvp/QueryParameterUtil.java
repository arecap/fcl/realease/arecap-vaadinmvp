/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouteConfiguration;
import org.arecap.cop.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public final class QueryParameterUtil {

    private final static Logger logger = LoggerFactory.getLogger(QueryParameterUtil.class);


    public static final <T> Optional<T> getQueryParameter(String name, Class<T> parameterType) {
        Optional<List<String>> parameterComposite = Optional
                .ofNullable(VaadinClientUrlUtil.getQueryParameters().getParameters().get(name));
        if(!parameterComposite.isPresent()) {
            logger.warn("parametrul " + name + "  nu este in definit");
            return Optional.empty();
        }
        String parameterStr = "";
        if(parameterComposite.get().size() != 1) {
            logger.warn("parametrul " + name + "  este setat gresit");
            return Optional.empty();
        }
        parameterStr = parameterComposite.get().get(0);
        ConversionService conversionService = (ConversionService) BeanUtil.getBean(ConversionService.class);
        if(!conversionService.canConvert(String.class, parameterType)) {
            logger.warn("valoarea\t"+ parameterStr +" a parametrului " + name + "  nu poate fi convertit la tipul cerut\t"+parameterType);
            return Optional.empty();
        }
        return Optional.of(conversionService.convert(parameterStr, parameterType));
    }

    public static final Optional<String> getQueryParameter(String name) {
        Optional<String> encodingValue = getQueryParameter(name, String.class);
        try {
            if(encodingValue.isPresent()) {
                return Optional.of(URLDecoder.decode(encodingValue.get(), StandardCharsets.UTF_8.toString()));
            }
        } catch (Throwable e) {

            logger.warn("decode error", e.getStackTrace());
        }
        return encodingValue;
    }

    public static final String getRelativePathWithQueryParameter(String name, Object parameter) {
        ConversionService conversionService = (ConversionService) BeanUtil.getBean(ConversionService.class);
        if(conversionService.canConvert(parameter.getClass(), String.class)) {
            if(VaadinClientUrlUtil.getRouteRelativePath().contains("pagina=")){
                return VaadinClientUrlUtil.getRouteRelativePath().
                        replaceFirst("\\b"+name+"=.*?(&|$)", "") + "&" + name + "=" +
                        conversionService.convert(parameter, String.class);
            } else {
                return VaadinClientUrlUtil.getRouteRelativePath() + "&" + name + "=" + conversionService.convert(parameter, String.class);
            }
        } else {
            logger.warn("parametrul de tipul\t"+ parameter.getClass() + " nu poate este serializabil!");
        }
        return VaadinClientUrlUtil.getRouteRelativePath();
    }

    public static final String getRelativePathWithQueryParameters(Map<String, Object> parameters, Class<? extends Component> route) {
        ConversionService conversionService = (ConversionService) BeanUtil.getBean(ConversionService.class);
        if(parameters == null || parameters.size() == 0) {
            return RouteConfiguration.forApplicationScope().getUrl(route);

        }
        StringBuilder parameterQuery = new StringBuilder("?");
        parameters.keySet().forEach(parameter -> {
            if(conversionService.canConvert(parameters.get(parameter).getClass(), String.class)) {
                parameterQuery.append(parameter + "=" + conversionService.convert(parameters.get(parameter), String.class) + "&");
            } else {
                logger.warn("parametrul de tipul\t"+ parameter.getClass() + " nu poate este serializabil!");
            }
        });
        return RouteConfiguration.forApplicationScope().getUrl(route) + parameterQuery.substring(0, parameterQuery.length() - 1).toString();
    }

}
