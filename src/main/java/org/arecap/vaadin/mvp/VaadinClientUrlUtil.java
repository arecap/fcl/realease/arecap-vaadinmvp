/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public final class VaadinClientUrlUtil {

    public static final String getProtocol() {
        return (((VaadinServletRequest) VaadinService.getCurrentRequest()).getProtocol().toLowerCase().contains("https") ? "https" : "http");
    }

    public static int getPort() {
        return ((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerPort();
    }

    public static final String getDomainPath() {
        return getProtocol()+"//"+((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerName() +
                (getPort() == 80 ? "" : ":" + ((VaadinServletRequest) VaadinService.getCurrentRequest()).getServerPort());
    }

    public static final String getRoutePath() {
        return getDomainPath() + "/" + UI.getCurrent().getInternals().getLastHandledLocation().getPathWithQueryParameters();
    }

    public static final String getRouteRelativePath() {
        return UI.getCurrent().getInternals().getLastHandledLocation().getPathWithQueryParameters();
    }

    public static final QueryParameters getQueryParameters() {
        return UI.getCurrent().getInternals().getLastHandledLocation().getQueryParameters();
    }

}
