/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.vaadin.mvp;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.upload.SucceededEvent;
import org.arecap.cop.BeanUtil;
import org.springframework.util.Assert;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public final class DomEventViewSupport {

    public static void registerClickEvent(PassiveView view, String presenterMethod, Component component, Object... params) {
        registerDomEvent(view, presenterMethod, component, ClickEvent.class, params);
    }

    public static  void registerUploadEvent(PassiveView view, String presenterMethod, Component component, Object... params) {
        registerDomEvent(view, presenterMethod, component, SucceededEvent.class, params);
    }

    public static  void registerComponentValueChangeEvent(PassiveView view, String presenterMethod, Component component, Object... params) {
        registerDomEvent(view, presenterMethod, component, AbstractField.ComponentValueChangeEvent.class, params);
    }

    public static  void registerDomEvent(PassiveView view, String presenterMethod, Component component, Class<? extends ComponentEvent> eventType, Object... params) {
        Assert.isAssignable(Component.class, view.getClass());
        ((PassiveViewComponentBuilder)BeanUtil.getBean(PassiveViewComponentBuilder.class)).registerDomEvent(((Component)view).getId().get(), presenterMethod, component, eventType, params);
    }
}
